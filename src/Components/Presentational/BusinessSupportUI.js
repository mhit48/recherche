import React from 'react'
const BusinessSupportUI = ()=>{
    return(
        <div className = 'whatWeDo'>
            <div className = 'whatWeDoBanner'>
                <div className = "whatWeDoHead">
                    <h2>Business Support</h2>
                    <p>
                    Business Support services at Recherché help free up your time so that you can optimize your productivity and can spend more time on your business critical functions. We think of your business as ours and are ready to take up any tasks that you delegate to us. Our team willingly learns on the job to replicate your systems and processes to ensure high efficiency and satisfaction levels for its clients.
                    </p>
                </div>
                
                <div className = "whatWeDoBtn">
                   <a href = "#payroll"><button>PAYROLL PROCESSING</button></a>
                   <a href = "#supplier"><button>SUPPLIER VULNERABILITY STUDIES</button></a> 
                   <a href = "#businessAssociate"><button>	BUSINESS ASSOCIATE RISK ASSESSMENT</button></a>
                   <a href = "#legal&regulatory"><button>LEGAL AND REGULATORY COMPLIANCES</button></a> 
                   <a href = "#policyManual"><button>POLICY MANUAL PREPARATION</button></a>
                   <a href = "#legalProcess"><button>LEGAL PROCESS OUTSOURCING</button></a>  

                </div>
            </div>
            <div className = 'whatWeDoContentList'>
               
                <div className = 'listItem' id ='payroll'>
                    <div className = 'itemHead'>
                        <p>PAYROLL PROCESSING</p>
                    </div>
                    <div className = 'itemContent'>
                    Payroll process outsourcing has become a critical management tool for cost containment and ensuring streamlined and error free payroll to its employees. Recherché provides payroll management and administration services where it takes care of all payroll processing requirements. Through its simple and convenient solutions we ensure correct and timely payment of salaries to employees as well as assist in regulatory compliances by employees and company including tax payment and returns, retirement benefits and compliance with other labour law requirements.


                    </div>
                </div>
                <div className = 'listItem' id = 'supplier'>
                    <div className = 'itemHead'>
                        <p>SUPPLIER VULNERABILITY STUDIES</p>
                    </div>
                    <div className = 'itemContent'>
                    Supply lines today are longer, and more complex than ever and it is vital to proactively identify ‘at-risk’ opportunities in your supplier base to prevent costly delays and disruptions. Supplier Vulnerability Study or Vendor Risk Assessment by Recherché can help you quickly identify critical supplier risks be it in terms of financial health, investment capabilities, technical competence, operational vulnerability, managerial expertise etc. that can disrupt your organization’s supply chain or a whole new product launch. 
                    </div>
                </div>
                <div className = 'listItem' id ="businessAssociate" >
                    <div className = 'itemHead'>
                        <p>	BUSINESS ASSOCIATE RISK ASSESSMENT</p>
                    </div>
                    <div className = 'itemContent'>
                    Business Associate Risk Assessment and Due Diligence services from Recherché helps you with a 360-degree evaluation of your new or existing business associate relationships like dealers, franchisee, and agents etc., to enable find best fit for your business. We evaluate the Associates on their financial, managerial, and technical and infrastructure capabilities to help select the right set of business associates that minimize your exposure and maximize your business.
                    </div>
                </div>
                <div className = 'listItem' id ='legal&regulatory'>
                    <div className = 'itemHead'>
                        <p>LEGAL AND REGULATORY COMPLIANCES</p>
                    </div>
                    <div className = 'itemContent'>
                    Recherché assists its clients in meeting the legal and regulatory compliance framework to help them become compliant and transparent in addressing the challenges of this dynamic regulatory environment. From assisting in entity incorporation, periodic regulatory filings, employee tax and statutory dues payment and compliance to any other legal, tax or regulatory compliance related work, our teams of experts and advisors help clients understand the risks and meet their obligations.   
                    </div>
                </div>
                <div className = 'listItem' id = 'policyManual'>
                    <div className = 'itemHead'>
                        <p>POLICY MANUAL PREPARATION</p>
                    </div>
                    <div className = 'itemContent'>
                        Our business process documentation services assists in preparing operation manuals for your business to ensure a standardized set of policies and procedures are followed across the organization. Manuals are developed around your company’s own reviewed and amended procedures and culture to ensure it fits in well with the company and employee’s work culture.
                    </div>
                </div>
                <div className = 'listItem' id = 'legalProcess'>
                    <div className = 'itemHead'>
                        <p>LEGAL PROCESS OUTSOURCING</p>
                    </div>
                    <div className = 'itemContent'>
                        Corporate legal business units and law firms are experiencing intense pressure to keep their legal expenses down as a part of all round business imperatives to become leaner and productive. We at Recherché India, a Knowledge Management Company, provide high-quality and cost effective legal support services. Our services span areas including Legal Research and Drafting, Contract Management and Review, Document Review, Paralegal Services, Legal Data Entry and Accounting and Secretarial & Regulatory Compliance and so on. We work closely with our clients providing cost-cutting possibilities and allowing them to focus on strategic legal issues and use their high-value resources more effectively.
                    </div>
                </div>
          
            </div>
        </div>
    )
}
export default BusinessSupportUI;