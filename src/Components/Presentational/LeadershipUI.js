import React from 'react';
import '../../css/leadership.css'
const LeadershipUI =()=>(
    <div className ='page'>
        <div className = 'pageHead'>
            <h1> Leadership</h1>
        </div>
        <div className = "pageContent">
            <div className = 'leadershipList'>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                            Vivek Srivastava, MRICS
                        </div>
                        <p>Founder & Managing Partner</p>
                    </div>
                    <div className = 'listItemDesc'>
                        <p>
                            Vivek holds Post-Graduate Diploma in Business Administration from Narsee Monjee Institute of Management Studies (NMIMS), Mumbai and is Member of Royal Institution of Chartered Surveyors (MRICS), London. 
                        </p>
                        <p>
                            Vivek brings more than 25 years of experience spanning Credit Rating, Corporate Risk Assessment, Risk Management and Mitigation Strategies, Real Estate Valuations, Securities and Financial Asset Valuations, Equity and Credit Research, Financial and Business Research Outsourcing, Corporate Due Diligence, Feasibility and Market Studies etc. Vivek has worked with clients that included national and international Banks, Financial Institutions, Investment Banks, Independent Research Companies, Brokerage Houses, Consulting and Advisory Firms, Investor Relation Firms etc., across different geographies including India, USA, UK, Middle East and Africa regions.
                        </p>
                        <p>
                            Vivek co-founded Recherché India Advisors LLP in 2013, and was co-founder of 3iAnalysis LLC in 2005 and ASP Research Service Pvt. Ltd in 2004 and was designated COO & Director of Research. Previously, he was designated as Director at FitchRatings India Private Limited and led the analytical team at Thomson Financial BankWatch (India). He was also Head of Ratings at Onicra Credit Rating Agency of India and has worked at the South Asia Regional Office (SARO) of International Development Research Centre (IDRC) – a Canadian Crown Corporation, among others.
                        </p>
                    </div>
                </div>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                        	Richa Kalia, FCS
                        </div>
                        <p>Founder & Managing Partner</p>
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        Richa brings a wealth of experience of nearly decade and a half in the areas of financial, business and market research. She has been instrumental in setting-up and leading teams that serviced analysts and research teams of international investment banking and brokerage houses as well as independent research providers and investor relation companies.
                    </p>
                    <p>
                        In addition to Richa’s expertise in financial research domain, her other areas of specialization include Corporate, Legal and Regulatory Compliances, Transaction Documentation, Secretarial Due Diligence, Legal Support Services etc. Richa has been involved in carrying out in-depth research on crypto markets and digital assets and is a cryptocurrency strategist in her own right.
                    </p>
                    <p>
                        Richa is highly qualified. She is a Fellow Member of Indian Institute of Company Secretaries of India (FCS, ICSI) and holds a Bachelor of Law (LLB) and Bachelor of Commerce degrees from Delhi University. She also holds a Post-Graduate Diploma in Business Administration from Symbiosis, Pune as well as Diploma in Cyber Law from Amity University, NOIDA. Previously Richa has been associated in various capacities at 3iAnalysis LLC and was last designated as Associate Director. 
                    </p>
                    </div>
                </div>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                                                    
                        	Aseem Kalia, FCA

                        </div>
                        <p> Managing Partner, Real Estate Advisory</p>
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        Aseem leads the Real Estate Advisory initiative at Recherché. He is Fellow Member of Institute of Chartered Accountants of India and brings a wealth of extensive experience spanning over 18+ years of which more than a decade relates to experience in all spheres of real estate business. His gamut of knowledge in real estate covers corporate planning, land acquisition and disposals, collaboration and joint development agreements, institutional fund raising, contract controls, taxation matters, Liaisoning with government and regulatory departments etc.
                    </p>
                    <p>
                        Aseem was associated Emaar India Group, a leading real estate development company for over a decade, in various roles. Other than the real estate he also specializes in the areas of corporate finance, direct and indirect taxation, accounting and audit, fund raising, deal structuring etc.
                    </p>
                    <p>
                        Prior to Emaar, Aseem has previously worked with Petroleum Development of Oman-Shell Group Company, PVR Limited, Pepsi Frito-Lays (India) etc.
                    </p>
                    </div>
                </div>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                        	Manpreet Kaur Sehdev

                        </div>
                        <p>Managing Partner, Research Solutions</p>
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        Manpreet is an accomplished research and analytics professional with 12+ years of experience in the areas of financial, business and market research solutions and services. Her niche areas of expertise are the bespoke research and content development domains wherein she has worked on numerous projects and has built and trained teams as well.
                    </p>
                    <p>
                        Manpreet also has extensive experience in conducting financial feasibility studies, supplier vulnerability studies, entity and business associate grading consulting projects. She has also carried out business associate and supplier due diligence and relative assessment of business schools and educational institutions.
                    </p>
                    <p>
                        Manpreet holds a Post Graduate Diploma in Business Management with specialization in Finance from ITM University, Warangal and Bachelor in Science – Information Technology & Computers from Guru Nanak Dev University. She has previously been associated with Karvy Global Limited, 3iAnalysis LLC and Onicra Credit Rating Agency of India Limited and has worked with both international and domestic clients.
                    </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
);
export default LeadershipUI;