import React, { Component } from 'react';
import Pdf_one from './../../static/Pdf/BTC.pdf';
import pdf_two from './../../static/Pdf/GST.pdf';
import pdf_three from './../../static/Pdf/Facebook_libra.pdf';
import pdf_four from './../../static/Pdf/cert.pdf';
import bit from './../../static/images/Bitcoin.jpg';
import big from './../../static/images/Big.JPG';
import libra from './../../static/images/Libra.JPG';
import richa from './../../static/images/richa.JPG';
const BlogUI =()=>(
    <div>
        <div className="blogs-banner">
            <div className="blogs-header">
                <h1>Blogs</h1>
            </div>
        </div>

        <div className="blogs-content">
            <div className="blogs-list">

                <div className="blogs-item">

                    <div className="blogs-img">
                        <img src = {bit} alt = 'oops'/>
                    </div>

                    <div className="blogs-pdf">
                    <h3><a href = {Pdf_one} target = "_blank">View</a></h3>
                    </div>

                </div>

                <div className="blogs-item">

                    <div className="blogs-img">
                        <img src = {big} alt = 'oops'/>
                    </div>
                    <div className="blogs-pdf">
                        <h3><a href = {pdf_two} target = "_blank">View</a></h3>
                    </div>

                    
                </div>

                <div className="blogs-item">

                    <div className="blogs-img">
                        <img src = {libra} alt = 'oops'/>
                    </div>

                    <div className="blogs-pdf">
                        <h3><a href = {pdf_three} target = "_blank">View</a></h3>
                    </div>

                    
                </div>

                <div className="blogs-item">

                    <div className="blogs-img">
                        <img src = {richa} alt = 'oops'/>
                    </div>

                    <div className="blogs-pdf">
                        <h3><a href = {pdf_four} target = "_blank">View</a></h3>
                    </div>


                    
                </div>

            </div>
        </div>
        
    </div>
);
export default BlogUI;
