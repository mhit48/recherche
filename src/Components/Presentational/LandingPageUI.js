import React from 'react';
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
const LandingPageUI = (props) =>(
    <div>
        {console.log(props.images)}
        <div className = 'banner'>
            <div className = 'bannerItem'>
                <div className = 'bannerHead'>
                    <h2>Recherché India</h2>
                </div>
                <div className = 'bannerContent'>
                    
                        <ul>
                        <li>Research, Advisory, Data and Support services
                            outsourcing specialists.</li>
                        <p></p>
                        <li>Experienced talent pool lead by specialist leadership.</li><p></p>
                        <li>Bespoke, customized or ready to use solutions offered.</li><p></p>
                        <li>Scalable, economical and quality outsourcing models.</li><p></p>
                        <li>An ISO 9001:2015 Certified Company.</li>
                        </ul>

                  
                </div>
            </div>
        </div>
        <div className = 'services'>
            <div className= 'serviceHead'>
                <h2>Services</h2>
            </div>
            <div className = 'serviceList'>
                <div className = 'serviceItem'>
                
                    <div className = 'serviceItemHead'>
                    <i className ="fa fa-hand-o-right"></i> Research & Advisory
                    </div>
                    <div className = 'serviceItemContent'>
                        <p>Provide bespoke and customized financial, business and investment research and advisory services that enable clients augment their in-house research teams leaving them to focus on idea generation, client management and portfolio optimization activities.</p>
                       
                    </div>
                </div>
                
                <div className = 'serviceItem'>
                    <div className = 'serviceItemHead'>
                    <i className ="fa fa-hand-o-right"></i> Real Estate Advisory
                    </div>
                    <div className = 'serviceItemContent'>
                        Leveraging a unique combination of accounting, taxation, regulatory and real expertise, Recherche Realty provides comprehensive support in real estate investments and transactions, asset and portfolio management and optimization, regulatory and financing advisory and related technical and functional support services.
                    </div>
                </div>
                <div className = 'serviceItem'>
                    <div className = 'serviceItemHead'>
                    <i className ="fa fa-hand-o-right"></i> Data Management Solution
                    </div>
                    <div className = 'serviceItemContent'>
                        Through its specialized data entry and management services arm “DataXpertz”, offering a diverse range of data and outsourcing servicing, from Data Entry, Data Capture, Forms Processing, Image Data Entry to Accounting and E-Bookkeeping, Financial Statement Spreadsheets etc. 
                    </div>
                </div>
                <div className = 'serviceItem'>
                    <div className = 'serviceItemHead'>
                    <i className ="fa fa-hand-o-right"></i> Support Service Solutions
                    </div>
                    <div className = 'serviceItemContent'>
                        Our Business Support services help free up your time from non-core, non-critical activities and thus enable you to focus on more business critical functions. We offer support services like payroll processing, vulnerability studies, regulatory compliances or content development services including company profiles, pitch-books and presentations etc. 
                    </div>
                </div>
            </div>
        </div>
        <div className = 'aboutUs'>
            <div className = 'aboutUsHead'>
                <h2>About Us</h2>
            </div>
            <div className = 'aboutUsContent'>
                <div className = 'content'>
                    Recherché India Advisors LLP is a leading knowledge management company that has developed capabilities in delivering customized, high-value and insightful research, advisory and outsourcing services and solutions, catering especially to the requirements of international clients and quality. We craft bespoke services and solutions for our clients and partners, leveraging on the pooled intellectual capital of our team.
                    Incorporated in 2013 in its current avatar at New Delhi, India, the genesis of Recherché lies in its founders congregating an experienced core team that brings years of experience in .......
                    
                </div>
                <div className = 'aboutUsbtn'>
                    <a href = 'about-us'>
                        <button> Know More Here....</button>
                    </a>
                </div>
            </div>
        </div>
        <div className = 'client'>
            <div className = 'clientHead'>
                <h2>Our clients </h2>
            </div>
            <div className = 'clientContent'>
                <div className = 'clientImageSlide'>
                     <div className ="slider" >
                        { props.images.map((item, key) =>
                            
                            <img  key = {key} src = {item}/>
                        )}
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
)
export default LandingPageUI;