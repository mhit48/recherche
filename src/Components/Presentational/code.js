
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCvn50PeTX2YXPl4rCKbJ5fHrsmSMowG_o",
    authDomain: "recherche-f0e70.firebaseapp.com",
    databaseURL: "https://recherche-f0e70.firebaseio.com",
    projectId: "recherche-f0e70",
    storageBucket: "recherche-f0e70.appspot.com",
    messagingSenderId: "909908491372",
    appId: "1:909908491372:web:6f788e35abfc341f"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  var messagesRef = firebase.database().ref('messages'); 







document.getElementById('contactForm').addEventListener('submit',submitForm);

function submitForm(e){
    e.preventDefault();

    var fname =  getInputVal('fname');
    var lname =  getInputVal('lname');
    var email =  getInputVal('email');
    var message =  getInputVal('message');

    saveMessage(fname,lname,email,message);

}

function getInputVal(id){

    return document.getElementById(id).value;
}

function saveMessage(fname,lname,email,message){
    var newMessageRef = messagesRef.push();
    newMessageRef.set({

        fname : fname,
        lname : lname,
        email : email,
        message : message
    });
}