import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import '../../css/contact.css'
 
const AnyReactComponent = ({ text }) => <div>{text}</div>;
 
class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: 28.6300,
      lng: 77.0809
    },
    zoom: 11
  };
 
  render() {
    return (
      // Important! Always set the container height explicitly

      <div className="map">
      {/* <div style={{ height: '50vh', width: '30%' }}> */}
        <GoogleMapReact
          bootstrapURLKeys={{ key:'AIzaSyCrFzlJ_nSgxW34U7F3KFiPsmoEsq2wJzg' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={28.6300}
            lng={77.0809}
            text="My Marker"
          />
          <Marker position={{ lat: 28.6300, lng: 77.0809}} />
        </GoogleMapReact>
      {/* </div> */}
      </div>
    );
  }
}
 
export default SimpleMap;