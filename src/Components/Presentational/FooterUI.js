import React from 'react'
const FooterUI = ()=>(
    <footer>
        <div className = 'footer'>
            <div className = 'footerList'>
                <div className = 'footerSocialNetwork'>
                    <i class="fa fa-facebook-square fa-2x"></i>
                    <i class="fa fa-twitter-square fa-2x"></i>
                    <i class="fa fa-linkedin fa-2x"></i>
                </div>

                <div className = 'footerright'>
                <li><a href="insights">Home</a></li>
                <li><a href="aboutUs">Who Are We</a></li>
                <li><a href="research">What we do</a></li>
                <li><a href="contact-us">Contact Us</a></li>
                <li><a href="careers">Careers</a></li>
                </div>
            </div>
        </div>
    </footer>
)
export default FooterUI;