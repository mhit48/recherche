import React from 'react'
import '../../css/experienceCapital.css'
const ExperienceCapitalUI =()=>(
    <div className ='page  experiencePage'>
        <div className = 'pageHead'>
            <h1> Experience Capital</h1>
        </div>
        <div className = "pageContent">
            <div className ='experienceList'>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                            G.C. Dwivedi
                        </div>
                        
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        GC possesses nearly four decades of experience in the petrochemical and fertilizer sectors. His areas of specialization include Erection, Commissioning, Operation and Maintenance of Fertilizer, Oil & Gas installations, Gas based Petrochemical Plant.
                    </p>
                    <p>
                        His strong background in project conceptualization, execution and management as well as his in-depth knowledge of Petrochemical industry, State-of-the-art technologies, Quality Management Systems, Safety procedures etc., provides expertise in ascertaining project and technology feasibility and success.
                    </p>
                    <p> 
                        GC has worked in various capacities with Gas Authority of India Limited (GAIL) and was last designated as Executive Director & Officer-in-Charge for the Petrochemical Project at Pata, Auraiya. He also worked with National Fertilizers Limited and also has experience of working global vendors and consultants. Post his retirement from GAIL, he has also been associated in advisory capacity with private and public sector entities.
                        GC holds a Bachelor of Engineering in Electrical from Thapar Institute of Engineering & Technology.
                    </p>

                    </div>
                </div>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                                                    
                            Vikas Katyal, FCA

                        </div>
                        
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        Vikas is a Fellow Member of Institute of Chartered Accountants of India (ICAI) and brings rich experience of two decades in the areas of accounting, audit, taxation, secretarial and regulatory compliance, corporate affairs etc.
                    </p>
                    <p>
                        Vikas specializes in the matters of direct and indirect taxes, deal structuring, corporate due diligence, forensic audit and company law matters. He has worked with clients from diverse industry sectors and structures including listed and unlisted entities, small and medium enterprises (SMEs), multi-national companies (MNCs), subsidiaries of foreign companies, liaison offices and non-for-profit organizations.
                    </p>
                    <p>
                        Vikas has been in practice for over two decades advising clients across India. He also specializes in accounting, payroll and e-bookkeeping outsourcing projects and assignments for both domestic as well as international clients.
                    </p>

                    </div>
                </div>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                        	Dinesh Seth, IRP
                        </div>
                       
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        Dinesh is a management consultant and Insolvency Resolution Professional with over one and a half decade of entrepreneurial experience in deal origination, deal structuring and deal completion for entities in the auto components, steel, paper, textiles, brewery, real estate, education and food processing sectors.
                    </p>
                    <p>
                        Dinesh specializes in restructuring and turning around mid-market companies by preparing revival and rehabilitation plan and arranging for necessary resources for revival of distressed entities. His other areas of expertise include project financing, debt syndication, treasury and forex advisory and credit risk assessment. He has been involved in fund raising transactions aggregating over INR 10 billion for MSME as well as big ticket debt syndication.
                    </p>
                    <p>
                        Dinesh is also promoter and principal at Sarvatra Consultants – a financial consulting and advisory firm and is also Executive Director at 360 Degree Consultancy Private Limited. Previously he has been associated with ICICI Bank Limited and Housing and Urban Development Corporation (HUDCO). He also runs a non-profit organization – Sarvatra Foundation, that works for promotion of education, health and employed in under developed areas.
                    </p>
                    <p>
                        Dinesh holds Post-Graduate Diploma in Business Administration from Narsee Monjee Institute of Management Studies (NMIMS), Mumbai and Bachelor of Law (LLB) and Bachelor of Commerce (B.Com.) degrees from Panjab University. 
                    </p>
                    </div>
                </div>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                        	Navneet Chugh
                        </div>
                       
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        Navneet brings wealth of experience in the real estate sector. He has nearly two decades of experience in the areas of real estate advisory, land-use planning, real estate valuations, real-estate project and portfolio feasibility studies and valuations etc.
                    </p>
                    <p>
                        He also specializes in rental assessments, highest and best-use (HBU) studies, business location advisory, market and consumer demand assessment studies, property disposal and exit strategies. 
                    </p>
                    <p>
                        Navneet is the founder director at NAV Technocrats Private Limited, a leading mortgage valuation and real estate advisory firm in north India, which associated with the leading mortgage lenders in the public and private sector banks and non-banking financial companies sector. Previously he was associated with ICICI Home Finance Company Limited, Grasim Industries and JK White Cement.
                    </p>
                    <p>
                        Navneet holds a Bachelor of Engineering (B.E.) in Civil Engineering from Punjab Engineering College and a Masters in Business Administration (MBA) from Panjab University. He is also Member of The Royal Institute of Chartered Surveyors (MRICS), London.
                    </p>
                    </div>
                </div>
                <div className = 'listItem'>
                    <div className = 'listItemImage'>
                        <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
                        <div className = 'imgHead'>
                        	Alok Tandon
                        </div>
                       
                    </div>
                    <div className = 'listItemDesc'>
                    <p>
                        Alok is an IT professional with vast experience spanning over 25 years in the fields of IT hardware, software, cloud computing, system security, internet risk assessment and mitigation strategies, VPN and CRM solutions etc.
                    </p>
                    <p>
                        Alok specializes in system integration, security and IT architecture designing, data and system migrations, cloud computing and security solutions and virtual private networks. He has successfully executed large assignments and projects for leading corporate houses and government agencies including the Income Tax Department, Election Commission of India.
                    </p>
                    <p>
                        Alok is currently Founder Director at Interlink Information Systems Private Limited, a company he founded in 1995. He holds a Bachelor of Engineering (B.E.) in Computer Science from Savitribai Phule Pune University and has completed a number of IT related certification courses.
                    </p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
)
export default ExperienceCapitalUI;