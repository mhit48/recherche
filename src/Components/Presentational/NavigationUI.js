import React from 'react';
import Logo from '../../static/images/newlogo.jpg';
import Image from '../../static/images/isoImage.png';
const NavigationUI = (props)=>(
    <nav className="navbar navbar-default">
    <div className="container-fluid">
      <div className="navbar-header">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <div className="lo">
          <div className="navbarbrand">
        <a className="navbar-brand" href="/"><img src = {Logo} alt = 'Recherche'/></a>
        </div>
        <div className="navbar_iso">
        <a className="navbar-iso_logo" href="/"><img src = {Image} alt = 'Recherche'/></a>
        </div>
        </div>
        
      </div>
  
      {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
      
      <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul className="nav navbar-nav navbar-right">
          <li className=""><a href="/">Home</a></li>
          <li className="dropdown">
          <a className="dropdown-toggle" data-toggle="dropdown" href="#">Who are we
          <span className="caret"></span></a>
          <ul className="dropdown-menu">
          <li><a href="/about-us">About Us</a></li>
          <li><a href="/leadership">Leadership</a></li>
          <li><a href="exp-level">Experience Capital</a></li>
          <li><a href="purpose-values">Passion & Values</a></li>
        </ul>
          </li>
        <li className="dropdown">
            <a className="dropdown-toggle" data-toggle="dropdown" href="#">What we do
            <span className="caret"></span></a>
            <ul className="dropdown-menu">
              <li><a href="/advisory">Advisory</a></li>
              <li><a href="/research">Research</a></li>
              <li><a href="/real_estate">Real Estate Advisory</a></li>
              <li><a href="https://www.dataxpertz.com">Data Entry & Management</a></li>
              <li className="dropdown"><a href = '#' data-toggle="dropdown" className="dropdown-toggle">Support Service Outsourcing <span className="caret"></span>
              </a></li>
                <ul >
                  <li><a href="businessSupport">Business Support</a></li>
                  <li><a href="contentDevelopment">Content Development</a></li>
                </ul>
              
            </ul>
        </li>
        <li className="dropdown">
          <a className="dropdown-toggle" data-toggle="dropdown" href="#">Insight
          <span className="caret"></span></a>
          <ul className="dropdown-menu">
          <li><a href="/Publication">Bespoke and samples</a></li>
          <li><a href="/Blog">Blog and publication</a></li>
          
        </ul>
          </li>
        
        <li><a href="contact-us">Contact Us</a></li>
        <li><a href="careers">Careers</a></li>
        </ul>
      </div>
    </div>
  </nav>
);
export default NavigationUI;