import React from 'react';
const ResearchUI =()=>{
    return(
        <div className = 'whatWeDo'>
            <div className = 'whatWeDoBanner'>
                <div className = "whatWeDoHead" >
                    <h2>Research</h2>
                    <p>
                        Recherché offers research and analytics outsourcing solutions and services to help clients outsource “bottom of the pyramid” functions and activities and concentrate on client facing, revenue generating and high-value ideas and activities. Our team with extensive experience in research, strategy consulting and risk management, we blend our expertise and processes to offer business, financial and investment research services to its global and domestic clients. Recherche enables its clients to retain the flexibility of deploying their senior staff for greater client facing roles, improve their analyst coverage ratio but without a drop in quality and depth, and achieve all this cost-effectively by outsourcing research support activities.

                    </p>
                </div>
                
                <div className = "whatWeDoBtn">
                   <a href = "#whiteLabel" ><button>BESPOKE WHITE LABEL RESEARCH</button></a>
                   <a href = "#blockchain"><button>BLOCKCHAIN & CRYPTOCURRENCY RESEARCH</button></a> 
                   <a href = "#equityResearch"><button>EQUITY RESEARCH </button></a>
                   <a href = "#fixedIncome"><button>FIXED-INCOME & CREDIT RESEARCH</button></a> 
                   <a href = "#earning"><button>EARNINGS & VALUATION MODELS</button></a> 
                   <a href = "#companySector"><button>COMPANY & SECTOR RESEARCH</button></a> 
                   <a href = "#investment"><button>INVESTMENT MEMORANDUMS & PITCH-BOOKS</button></a> 

                </div>
            </div>
            <div className = 'whatWeDoContentList'>
               
                <div className = 'listItem' id ='whiteLabel'>
                    <div className = 'itemHead'>
                        <p>BESPOKE WHITE LABEL RESEARCH</p>
                    </div>
                    <div className = 'itemContent'>
                        Recherché provides “custom-made” or “made to order” research services wherein it customizes the reports to the client’s requirement. These reports cut across verticals and could deal with analysis of impact of special events, policy changes, changes in the operating environment, Merger & Acquisition activity etc.
                        Or the companies may want to outsource part or entire research and analysis activities but they still want to sell or distribute the research reports or results featuring their own brand name. For such companies, Recherché undertakes the research, analysis, financial modeling, valuation, report writing, editing and delivers it to its clients the final report along with related model in client customized format, under client’s brand name which can be distributed by the client to its customers.
                        Team Recherché recognizes that it can add value by simply serving as an outsourced research solution provider to its clients that may not have the time or resources to complete certain specialized research projects. Recherché can either work as an extension of the client’s in-house research team helping them expand their coverage or as a completely outsourced research division of the client, publishing end-to-end research reports under the client’s brand.
                    </div>
                </div>
                <div className = 'listItem' id = 'blockchain'>
                    <div className = 'itemHead'>
                        <p>BLOCKCHAIN & CRYPTOCURRENCY RESEARCH</p>
                    </div>
                    <div className = 'itemContent'>
                        The blockchain technology and the new world of cryptocurrency that is based on blockchain technology are as volatile as they are exciting. It’s a whole new world there with new developments happening in the two segments by the hour. A large amount of both technical and commercial data is available out there, but there are still gaps when it comes to synthesized information and research on these developments. Recherché aims to bridge this gap by providing in-depth analysis and in-sights of the digital assets, overall crypto markets through its institutional grade research reports. 
                    </div>
                </div>
                <div className = 'listItem' id ="equityResearch" >
                    <div className = 'itemHead'>
                        <p>EQUITY RESEARCH </p>
                    </div>
                    <div className = 'itemContent'>
                        Producing high quality and timely research is a great way to differentiate in the market. Recherché provides a wide range of equity research outsourcing services and financial research outsourcing services to equity focused clients that help them accelerate the pace of idea generation, expansion into new geographies, entry into new sectors and on-going maintenance research, handle spike in work during earnings season thus allowing in-house analysts to spend more time on revenue generating activities.
                        Recherché helps sell-side firms to rapidly expand coverage and reduce time spend on routine maintenance research while it supports the buy-side firms by providing data-heavy research tasks in areas relating to investment screening, modeling, portfolio tracking etc. Our focus has been to provide consistent and scalable equity research solutions to our clients.

                    </div>
                </div>
                <div className = 'listItem' id ='fixedIncome'>
                    <div className = 'itemHead'>
                        <p>FIXED-INCOME & CREDIT RESEARCH</p>
                    </div>
                    <div className = 'itemContent'>
                        Recherché provides a wide range of fixed-income and credit research and analytics outsourcing solutions to both buy-side and sell-side clients, helping them identify and evaluate potential investment opportunities, and control their costs. We support our clients in credit modeling, initiating coverage, maintenance research, preparing and maintaining relative value databases, investment memos and notes, conducting analysis, portfolio tracking and several other activities thus freeing up their valuable time for revenue generation and client facing activities.
                        Our research capabilities are spread across most fixed-income products, including rates, investment-grade, high-yield, distressed debt, leveraged finance, structured products, public finance, emerging markets and commodities.

                    </div>
                </div>
                <div className = 'listItem' id = 'earning'>
                    <div className = 'itemHead'>
                        <p>EARNINGS & VALUATION MODELS</p>
                    </div>
                    <div className = 'itemContent'>
                        Recherché provides reliable financial modeling and company valuation support services that may even be customized to each client’s needs. Our services support fund managers and analysts in building and maintaining financial model across industry sectors and our capabilities include populating historical financial data, capital structure analysis models, comparable company analysis, simple and detailed company valuation models, detailed earnings estimation models, and incorporating sensitivity analysis etc.
                        By leveraging our capabilities, our clients save time, money and effort in building and maintaining financial models and allow them faster turnaround during earnings seasons and other corporate actions. We can build models on our proprietary templates or can create firm-specific standard templates and databases.
                    </div>
                </div>
                <div className = 'listItem' id = 'companySector'>
                    <div className = 'itemHead'>
                        <p>COMPANY & SECTOR RESEARCH</p>
                    </div>
                    <div className = 'itemContent'>
                        Recherché assists its client in preparing company and sector research reports that could range from short investment briefs to detailed research report covering investment rationale, industry dynamics and outlook, risks and concerns along with rating recommendations of “Buy”, “Sell”, “Hold” or “Accumulate” (depending on rating scale followed by the client). 
                        Sector reports cover detailed trends and development of the sector, operating environment, policy regime, risks and concerns and may include brief insights on major players in the sector and their business strategies. We can customize our services and reports to align with each client’s requirements and templates providing them “ready to publish and distribute” reports.
                    </div>
                </div>
                <div className = 'listItem' id = 'investment'>
                    <div className = 'itemHead'>
                        <p>INVESTMENT MEMORANDUMS & PITCH-BOOKS</p>
                    </div>
                    <div className = 'itemContent'>
                        Recherché’s investment memorandum and pitch-book services primarily support hedge funds, private equity funds and entities looking to raise funds from the market. Our team of analysts will do the research and analysis, build financial models and gather data to prepare investment memorandum and pitch-books that can be used by funds to circulate amongst its investors and decision makers. These can be effectively used as sales pitch for investments by companies and entities looking at doing road-shows and presentations for fund raising.
                    </div>
                </div>
                <div className = 'note'>
                    
                    If your exact research solution is not enlisted above, please 
                    <a href='contact-us'> contact us</a> and we will be glad to discuss your requirement and propose a tailor made solution to match your needs.
                        
                </div>
            </div>
        </div>
    )
}
export default ResearchUI;