import React from 'react'
const PurposeValueUI =()=>(
    <div className = 'purposeValue'>
        <div className = 'purposeValueDesc'>
            <div className = 'descHead'>
                Passion & Values
            </div>
            <div className = 'descContent'>
                Recherché strives towards passion for results and completely aligning with our clients for their success. We commit ourselves to highest quality, professionalism and ethical standards in everything we do. 
                Recherche believes its success is driven by success and satisfaction of its clients. We aim to nurture long-term mutually beneficial relationships with our clients by imbibing strong value system in our work ethics and dealings with all stakeholders.
                Our value system follows the 4-Cs:

            </div>
            <div className = 'purposeValueList'>
                <div className = 'pvListItem'>
                    <div className = 'listItemHead'>
                        Confidentiality
                    </div>
                    <div className = 'listItemContent'>
                        This is the foremost principle that guides our style of working. Every piece of information that our clients share with us is treated as sensitive and is kept confidential.
                    </div>
                </div>
                <div className = 'pvListItem'>
                    <div className = 'listItemHead'>
                        Commitment
                    </div>
                    <div className = 'listItemContent'>
                        Taking genuine interest in our clients’ business and giving our best to achieve the desired results is how we stay committed through the life of project and assignments. At times, our scope of work goes beyond what the clients sign us for. We truly understand this and are committed to achieve the deliverables.
                    </div>
                </div>
                <div className = 'pvListItem'>
                    <div className = 'listItemHead'>
                        Collaboration
                    </div>
                
                    <div className = 'listItemContent'>
                        Working as partners to our clients rather than just consultants, we aim to use a collaborative approach to problem solving. Further we take full ownership of the project and use all necessary resources possible to achieve our goals as set out in the strategy plan.
                    </div>
                </div>
                <div className = 'pvListItem'>
                    <div className = 'listItemHead'>
                        Creativity
                    </div>
                    <div className = 'listItemContent'>     
                        Each business problem is unique in its own way and requires genuine, creative and innovative solutions. Having cruised through solving business problems across number of clients and projects, we leverage this experience to bring forth innovative approach to problem solving.

                    </div>
                </div>
            </div>
        </div>
    </div>
);
export default PurposeValueUI