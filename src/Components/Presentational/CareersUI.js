import React from 'react'
const CareersUI = ()=>{
    return(
        <div className = 'careers'>
        
            <div className= 'careersList'>
                <div className = 'head'>
                    <h2>Career At Recherché</h2>
                </div>
                <div className = 'careersItem'>
                    <div className = 'careersHead'>
                        <h2>Life At Recherché</h2>
                    </div>
                    <div className = 'careersContent'>
                        <p>
                            Our biggest asset is our team of happy individuals. We care about our people, offer them a unique ecosystem that enables you to grow with us.
                            Recherché offers an environment where work and fun co-exist. We enjoy the process of working together and foster an environment that is fun-filled, vibrant and encourages people to achieve their potential.
                        </p>
                        <p>
                            Recherché is a young and dynamic organization that offers an engaging work culture that allows you to learn, grow and experience success with us.  
                            Our policies and practices are employee-centric and aligned with market practices. We communicate with our teams on continuous basis and strive to encourage them to achieve a healthy work-life balance.
                        </p>
                    </div>
                </div>
                <div className = 'careersItem'>
                    <div className = 'careersHead'>
                        <h2>Current Opening</h2>
                    </div>
                    <div className = 'careersContent'>
                        <div className ='contentHead'>
                            <i className ="fa fa-hand-o-right"> </i> <p> Process Associates – Data</p>
                        </div>
                        <p>
                            Fresh graduates in any stream or graduates with a maximum one-year experience to work on Data Management projects.
                        </p>
                        <div className ='contentHead'>
                            <i className ="fa fa-hand-o-right"></i> <p> Research Analysts </p>
                        </div>
                        <p>
                            Fresh MBA graduates or MBAs with 6 to 12 months experience with major in finance to work on financial and business research assignments. Good working knowledge of MS Excel and MS PowerPoint is essential. 
                        </p>

                    </div>
                    <p>
                        To apply please email us your resume to <a href = '#'> info@recherche-india.com </a>
                     </p>
                </div>
                <div className = 'careersItem'>
                    <div className = 'careersHead'>
                        <h2>Internships</h2>
                    </div>
                    <div className = 'careersContent'>
                        <p>
                        Recherché, from time to time, looks for exceptional individuals from premier educational institutions who are looking to gain hands-on industry experience in fields of financial research, advisory, data management and fintech domains.


                        </p>
                        <p>
                            Internships are typically of six to ten week durations and expose the individuals to real-world analytics, advisory and fintech related projects and assignments. Interns are given critical projects, assigned mentors and work with teams as part of this program. We may offer full-time position to high-potential talents looking to pursue a professional career in service verticals offered by Recherché. 
                        </p>
                        <p>
                            To apply for internship at Recherche, you may email us your resume and a brief note on the subject that you wish to work on to <a href ='#'> info@recherche-india.com </a>

                        </p>
                    </div>
                </div>
            </div>
        </div>
                         
    )
}
export default CareersUI