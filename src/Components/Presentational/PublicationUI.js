import React, { Component } from 'react';
import DownloadLink from "react-download-link";
import R1 from '../../static/images/R1.png';
import R2 from '../../static/images/R2.png';
import R3 from '../../static/images/R3.png';
import R4 from '../../static/images/R4.png';
import R5 from '../../static/images/R5.png';
import R6 from '../../static/images/R6.png';
import R7 from '../../static/images/R7.png';
import R8 from '../../static/images/R8.png';
import R9 from '../../static/images/R9.png';
export default class PublicationUI extends Component {
  render() {
    return (
        <div>
      <div class="sample_wrap">
          <div class="image1">
        <p></p>
          </div>
          <div class="text">
            <h1>Samples</h1>
          </div>
      </div>

      <div class="pdf_wrap">
        <div class="pdf1">
            <div class="image_pdf">
            {/* <DownloadLink
                filename="aman.txt"
                exportFile={() => "aman.txt"}
            >
            Save to disk
            </DownloadLink> */}

            <img src = {R1} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Investment Memorandum</h1>
            </div>
        </div>


        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R2} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>US Small Cap Equality Report</h1>
            </div>
        </div>

        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R3} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Spotlight Research</h1>
            </div>
        </div>

      </div>


      <div class="pdf_wrap">
        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R4} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Newsletters</h1>
            </div>
        </div>


        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R5} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Market Research Report</h1>
            </div>
        </div>

        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R6} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Land Valuation Report</h1>
            </div>
        </div>

      </div>

      <div class="pdf_wrap">
        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R7} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Sector Research Report</h1>
            </div>
        </div>


        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R8} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Cryptocurrency Coin Research</h1>
            </div>
        </div>

        <div class="pdf1">
            <div class="image_pdf">
            <img src = {R9} alt = 'Recherche'/>
            </div>
            <div class="image_heading">
                <h1>Cryptocurrency Market Research</h1>
            </div>
        </div>

      </div>

      
      </div>
    );
  }
}
