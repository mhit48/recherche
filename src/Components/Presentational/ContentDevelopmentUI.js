import React from 'react'
const ContentDevelopmentUI = ()=>{
    return(
        <div className = 'whatWeDo'>
            <div className = 'whatWeDoBanner'>
                <div className = "whatWeDoHead">
                    <h2>Content Development</h2>
                    <p>
                        The ability to write professional content that can hold the interest of readers at the first glance is a pretty tough task. Professional writers at Recherché discuss and understand your business, its requirement and your target audience. As a result, you will get exactly what you always wanted to say about your business in a crisp and comprehensive way that is accurate, simple and in-depth researched. Outsource content writing services to seasoned writers at Recherché who can add value to your project by producing authentic, well researched articles, presentations, newsletters and other corporate communications. 
                    </p>
                </div>
                
                <div className = "whatWeDoBtn">
                   <a href = "#investmentPresentation"><button>COMPANY & INVESTOR PRESENTATIONS</button></a>
                   <a href = "#pitchBookPresentation"><button>INVESTMENT PITCH-BOOK PRESENTATIONS</button></a> 
                   <a href = "#profileWriting"><button>COMPANY PROFILE WRITING</button></a>
                   <a href = "#newsletterWriting"><button>NEWSLETTER WRITING</button></a> 
                   <a href = "#blog&research"><button>BLOGS AND RESEARCH ARTICLES</button></a>

                </div>
            </div>
            <div className = 'whatWeDoContentList'>
               
                <div className = 'listItem' id ='investmentPresentation'>
                    <div className = 'itemHead'>
                        <p>COMPANY & INVESTOR PRESENTATIONS</p>
                    </div>
                    <div className = 'itemContent'>
                        In today’s business scenario, presenting and exploring more about your business has become a tremendous way to win your clients, taking the business meeting to another level and ensuring a captive audience. We help our clients in preparing crisp and clear presentations that may be aimed at its potential investors, lenders, customers, employees or other stakeholders. Our presentations use attractive designs conveying corporate branding, neat-looking statistical data with self-explanatory graphs that help win the audience effectively making the right impact you are looking for.

                    </div>
                </div>
                <div className = 'listItem' id = "pitchBookPresentation">
                    <div className = 'itemHead'>
                        <p>INVESTMENT PITCH-BOOK PRESENTATIONS</p>
                    </div>
                    <div className = 'itemContent'>
                        Raising money from investors for your business is challenging at any stage and requires a great pitch, even for experienced founders and promoters with significant traction in their company. A polished and professional investor-centric presentation deck sets our clients apart from other companies competing for capital and resources. Recherché works with you to understand you, your business, sector and market nuances and tailors information into a pitch to maximize positive reception from investors. Our investor decks are just not about the data and bullets to sway investors but telling your story to engage investor imagination and interest, in lieu of face to face meetings.


                    </div>
                </div>
                <div className = 'listItem' id ="profileWriting" >
                    <div className = 'itemHead'>
                        <p>COMPANY PROFILE WRITING</p>
                    </div>
                    <div className = 'itemContent'>
                    “First Impression is the Lasting Impression” and can be the deciding factor between opening or closing doors to your business opportunities. This is where company profile comes into play. It’s the first introduction to your prospective and future clients and business opportunities. Therefore every company needs a crisp, yet comprehensive company profile that is understandable not only to the domain experts but also to top decision makers and others, who may be non-technical or general management professionals. Recherché helps you in writing the company profile for you. We understand your business, products and services, growth strategies, engagement processes, your target clientele etc., to prepare a impressive yet a crisp company profile
                    </div>
                </div>
                <div className = 'listItem' id ='newsletterWriting'>
                    <div className = 'itemHead'>
                        <p>NEWSLETTER WRITING </p>
                    </div>
                    <div className = 'itemContent'>
                    Through our Newsletter Writing Services, Recherche helps you connect with your existing and prospective clients, subscribers, investors, vendors and suppliers, employees on a regular basis. This could be a great brand building and promotion activity for your business. Our pool of research analysts, data aggregators and copywriters are well adept at developing compelling content for your online or email newsletter. The newsletters could cover regular updates on stock markets, economy, sector and industry updates, mutual fund performances and NAVs, insurance products and markets, deals and transactions etc.   
                    </div>
                </div>
                <div className = 'listItem' id = 'blog&research'>
                    <div className = 'itemHead'>
                        <p>BLOGS AND RESEARCH ARTICLES</p>
                    </div>
                    <div className = 'itemContent'>
                    Businesses are increasingly using their blogs as potent public relations and marketing tools. Blogs can help you position as a domain leader in the minds of your existing and prospective clients while it also provides a platform to interact with your prospective customers. Under its Blog Writing services, Recherché can suggest you themes and ideas for your blogs, write blogs, post blogs as per agreed schedule and manage the entire blog strategy for your company.
                        Similar to blogs, research articles help companies attract more traffic to their website and are used as a marketing tool to showcase its research capabilities as well as provide stakeholders a glimpse of your company’s opinion on thought process on varied issues and subject. We can help you write research articles on behalf of your company and research teams.

                    </div>
                </div>
                
            </div>
        </div>
    )
}
export default ContentDevelopmentUI;