import React from 'react'
const AdvisoryUI = ()=>{
    return(
        <div className = 'whatWeDo'>
            <div className = 'whatWeDoBanner'>
                <div className = "whatWeDoHead">
                    <h2>Advisory</h2>
                    <p>
                        Recherché offers investment, financial and project advisory services to help its clients prepare for their new business ventures or expand their existing businesses. Our financial and investment advisory services are backed by years of experience and proven analytical expertise. We believe in a partnership approach and, as such, work towards building a long-term relationship with each client, who we believe is unique and has distinct needs.

                    </p>
                </div>
                
                <div className = "whatWeDoBtn">
                   <a href = "#fundRasing"><button>FUND RAISING</button></a>
                   <a href = "#Ibc"><button>IBC RESOLUTION PLAN ADVISORY</button></a> 
                   <a href = "#trasaction"><button>TRANSACTION ADVISORY</button></a>
                   <a href = "#businessdiligence"><button>BUSINESS DUE DILIGENCE</button></a> 
                   <a href = "#exitstrategy"><button>EXIT STRATEGY EVALUATION</button></a> 

                </div>
            </div>
            <div className = 'whatWeDoContentList'>
               
                <div className = 'listItem' id ='fundRasing'>
                    <div className = 'itemHead'>
                        <p>FUND RAISING</p>
                    </div>
                    <div className = 'itemContent'>
                        Money is undeniably the lifeblood of every business. Companies, both public and private, regardless of the stage of maturity, will require funds for daily operations, expansion, investment, acquisition, debt reduction as well as to strengthen their capital bases and increase liquidity for themselves or their shareholders. Recherché, through its relationships with investors and lenders, helps clients in fund raising through private equity, debt syndication, venture capital, seed funding, strategic partners, high-networth individuals etc. We put ourselves in the fund provider's shoes to ensure the capital structure and financing arrangement is sensible and workable. We will advise you on the appropriate funding amount and the sources of funds.
                    </div>
                </div>
                <div className = 'listItem' id = 'Ibc'>
                    <div className = 'itemHead'>
                        <p>IBC RESOLUTION PLAN ADVISORY</p>
                    </div>
                    <div className = 'itemContent'>
                        The Insolvency and Bankruptcy Code 2016 of India requires that a “resolution plan” be prepared by any person for insolvency resolution of the corporate debtor as a going concern. The resolution plan is prepared on the basis of information memorandum given by the Resolution Professionals and identifies the sources of funds to be used to cover the resolution process costs, liquidation value due to the operational creditors and liquidation value outstanding to dissenting financial creditors. The team of experts at Recherche, which includes CAs, CSs, Investment Bankers, Lawyers etc., help the resolution applicants and/or other stakeholders to prepare an appropriate resolution plan for distressed entity. 
                    </div>
                </div>
                <div className = 'listItem' id ="trasaction" >
                    <div className = 'itemHead'>
                        <p>TRANSACTION ADVISORY</p>
                    </div>
                    <div className = 'itemContent'>
                        Recherché works with strategic and financial buyers, sellers and lenders across the full spectrum of merger and acquisition engagements. Whether a client is looking to acquire a company, obtain or provide additional capital, or sell a business, we can help clients gain a better understanding of the target business. We focus on serving corporate strategic buyers, private equity firms and other financial investors for all business transaction needs. We concentrate on our client’s strategic objectives, negotiation and savings opportunities in defining the services appropriate to each transaction.
                    </div>
                </div>
                <div className = 'listItem' id ='businessdiligence'>
                    <div className = 'itemHead'>
                        <p>BUSINESS DUE DILIGENCE</p>
                    </div>
                    <div className = 'itemContent'>
                        Buyers, lenders and sellers involved in mergers or acquisitions must identify the risks and the opportunities associated with the business under consideration. Recherché provides business and financial due diligence to buyers, lenders and sellers in a variety of transactional situations. We assist strategic and financial buyers focused on historic and projected financial performance, developing a more complete picture of the financial realities of a target business by reviewing the quality of earnings, conduction integrated assessment and reveal undisclosed or understated liabilities.
                    </div>
                </div>
                <div className = 'listItem' id = 'exitstrategy'>
                    <div className = 'itemHead'>
                        <p>EXIT STRATEGY EVALUATION</p>
                    </div>
                    <div className = 'itemContent'>
                        Whether a business owner is looking to “someday” sell or keep the business indefinitely or transition to a management team or family member, Recherché helps owners evaluate and optimize their options to give owners choices for near term and long term.  Recherché conducts a detailed evaluation of the business and the personal goals of the owners and other key stakeholders and evaluate the current state and realistic versions of possible future states that align with the goals of the owner to maximize value to them. From identifying changes that will increase value and cash-flow, to developing projections for future growth, to connecting owners to potential buyers and facilitating a sale, Recherché presents businesses with exit choices that align with their goals and yield maximum value.
                    </div>
                </div>
          
            </div>
        </div>
    )
}
export default AdvisoryUI;