import React from 'react';
import aboutBanner from '../../static/images/about.jpg'
const AboutUsUI =()=>(
    <div>
        <div className = 'aboutPageBanner'>
            <img src = {aboutBanner} alt = 'about us'/>
            <h2> Who We Are...</h2>
        </div>
        <div className = 'aboutPage'>
            <div className = 'aboutPageContent'>
                <p>
                    Recherché India Advisors LLP is a leading knowledge management company that has developed capabilities in delivering customized, high-value and insightful research, advisory and outsourcing services and solutions, catering especially to the requirements of international clients and quality. We craft bespoke services and solutions for our clients and partners, leveraging on the pooled intellectual capital of our team.
                </p>
                <p>
                    Incorporated in 2013 in its current avatar at New Delhi, India, the genesis of Recherché lies in its founders congregating an experienced core team that brings years of experience in developing and delivering solutions in the areas of business and financial research, real estate advisory, data management, knowledge process outsourcing and business support services.
                </p>
                <p>
                    We work alongside our clients and partners, many a times as an extension of their businesses and teams, to provide solutions that enable a more informed decision making and supporting them by undertaking bottom-of-the-pyramid functions and processes thus ensuring that high-cost resources are focused on the revenue generating front-end activities.
                </p>
                <p>
                     The research and advisory and the support service outsourcing functions are housed within Recherché India Advisors LLP while we undertake real estate advisory under the Recherché Realty division and data entry and management services under the DataXpertz division.
                </p>
            </div>
        </div>
    </div>
);
export default AboutUsUI;
