import React, {Component} from 'react';
import LandingPageUI from '../Presentational/LandingPageUI';
import '../../css/landingPage.css'
class LandingPage extends Component{
  importAll(r) {                            //importing all images from static/images
    return r.keys().map(r);
  }
  
  
  render(){
      return(
        <LandingPageUI 
        const images = {this.importAll(require.context('../../static/images/clients', false, /\.(png|jpe?g|svg)$/))}/>  
      )
  }
}
export default LandingPage;
