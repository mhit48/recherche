import React, { Component } from 'react';
import { Route, Switch , BrowserRouter } from 'react-router-dom';
// import * as routes from "./UiConfigs/routes";
import './css/common.css'
import Navigation from './Components/Container/Navigation';
import LandingPage from './Components/Container/LandingPage'
import Footer from './Components/Container/Footer';
import AboutUs from './Components/Container/AboutUs';
import Leadership from './Components/Container/Leadership';
import PurposeValue from './Components/Container/PurposeValue';
import ExperienceCapital from './Components/Container/ExperienceCapital';
import Advisory from './Components/Container/Advisory';
import Research from './Components/Container/Research';
import Real from './Components/Container/Realestate';
import BusinessSupport from './Components/Container/BusinessSupport';
import ContentDevelopment from './Components/Container/ContentDevelopment';
import Careers from './Components/Container/Careers';
import Contact from './Components/Container/Contact';
import Publication from './Components/Container/Publication';
import Blog from './Components/Container/Blog';
// import Form from './Components/Container/Form';
import Map from './Components/Presentational/MapUI';
import * as firebase from 'firebase';
import '../src/Components/Presentational/FormUI';
import axios from 'axios';






class App extends Component{
  
    render(){
        return (
            <div className="appContainer">
               <BrowserRouter>
                <Navigation/>
                <Switch>
                  <Route
                    exact path = '/'
                    component = {LandingPage}
                  />
                  <Route
                    path = '/about-us'
                    component = {AboutUs}
                  />
                  <Route
                    path = '/leadership'
                    component = {Leadership}
                  />
                  <Route
                    path = '/exp-level'
                    component = {ExperienceCapital}
                  />
                  <Route
                    path = '/purpose-values'
                    component = {PurposeValue}
                  />
                  
                  <Route
                    path = '/advisory'
                    component = {Advisory}
                  />
                  <Route
                    path = '/research'
                    component = {Research}
                  />

                  <Route
                    path = '/real_estate'
                    component = {Real}
                  />
                  <Route
                    path = '/businessSupport'
                    component = {BusinessSupport}
                  />
                  <Route
                    path = '/contentDevelopment'
                    component = {ContentDevelopment}
                  />
                  <Route
                    path = '/publication'
                    component = {Publication}
                  />

                  <Route
                    path = '/Blog'
                    component = {Blog}
                  />
                  <Route
                    path = '/careers'
                    component = {Careers}
                  />
                  
                  <Route
                     path = '/contact-us'
                    component = {Contact}
                  />

                  
                </Switch>
                <Footer/>
            </BrowserRouter>
            </div>
          );
    }
}
export default App;
